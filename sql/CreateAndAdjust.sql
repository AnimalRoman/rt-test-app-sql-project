/*������ ��� ���������� ������ PSQL*/
/*=======������ �������������� � ��������� �� search_person==*/
/*====Created: Fedorov R.A. Creation Date: 19.06.2016. Last Change: 19.06.2016====*/

CREATE DATABASE search_person; /*������� ��*/
CREATE USER webappusr WITH password 'hjcntktrjvNtcn'; /*������� ��������� ������������, ������� ����� �������������� �������� �����������*/

/*������������ � ��������� ��*/
\connect search_person; 

/*������� ����� ��� ���-����������; ����� ����� ������������ ��������� ������������*/

CREATE SCHEMA web_application AUTHORIZATION webappusr; 
 /*��������� ��� ���� ����� �� ��������� �� ����� public*/
REVOKE ALL PRIVILEGES ON SCHEMA public FROM public;
/*��������� ��� ��������� ������������ ����� �� ��������� �� ����� ��� ���-����������*/
REVOKE ALL PRIVILEGES ON SCHEMA web_application FROM webappusr; 

 /*������� ������� � �����������*/

/*=================���������� �������=============*/
CREATE TABLE web_application.city (id SERIAL PRIMARY KEY, name VARCHAR(50) NOT NULL); 
/*������� ������, ��� ���� ����� ������������� ������������ �������� �������*/
CREATE UNIQUE INDEX indx_city_unique on web_application.city(upper(name)); 

/*=================���������� �����=============*/ 
CREATE TABLE web_application.car (id SERIAL PRIMARY KEY, name VARCHAR(100) NOT NULL);
/*������� ������, ��� ���� ����� ������������� ������������ �������� �����*/
CREATE UNIQUE INDEX indx_car_unique on web_application.car(upper(name)); 
 
/*=================������� �����=============*/ 
CREATE TABLE web_application.persons (id SERIAL PRIMARY KEY, surname VARCHAR(100) NOT NULL,
name VARCHAR(100) NOT NULL, patronymic VARCHAR(100) NULL, city_id INT NULL, car_id INT NULL,
FOREIGN KEY (city_id) REFERENCES web_application.city(id),
FOREIGN KEY (car_id) REFERENCES web_application.car(id));

/*=================������� ������������� ���-����������=============*/ 
CREATE TABLE web_application.users (id SERIAL PRIMARY KEY, login VARCHAR(100) NOT NULL,
password VARCHAR(100) NOT NULL, surname VARCHAR(100) NOT NULL,
name VARCHAR(100) NOT NULL, patronymic VARCHAR(100) NULL, enabled SMALLINT NOT NULL DEFAULT 0);
/*������� ������, ��� ���� ����� ������������� ������������ �������*/
CREATE UNIQUE INDEX indx_users_login_unique on web_application.users(upper(login)); 

/*=================������� ����� ��� ����������� ������������� ���-����������=============*/ 
CREATE TABLE web_application.user_roles (id SERIAL PRIMARY KEY, user_id INT NOT NULL,
role VARCHAR(100) NOT NULL,
FOREIGN KEY (user_id) REFERENCES web_application.users(id)
);
/*������� ������, ��� ���� ����� ������������� ������������ ���� - user_id-role*/
CREATE UNIQUE INDEX indx_user_roles_user_id_role_unique on web_application.user_roles(user_id, role); 

 
 /*===============�������� ��������� ������ �������� �� ���, ������ � ������===============================*/
 
CREATE FUNCTION web_application.persons_find_person
(_surname varchar(100), _name varchar(100), _patronymic varchar(100),
 _city varchar(50), _car varchar(100)) RETURNS TABLE (surname varchar(100), name varchar(100), patronymic varchar(100),
 city varchar(50), car varchar(100)) as 
 $$
 SELECT INR.surname, INR.name, INR.patronymic, INR.city, INR.car FROM
 (
SELECT COALESCE(PRN.surname,'') AS surname, COALESCE(PRN.name,'') AS name,
 COALESCE(PRN.patronymic,'') AS patronymic, COALESCE(CTY.name,'') as city, COALESCE(CAR.name,'') as car  FROM web_application.persons as PRN
LEFT OUTER JOIN web_application.city as CTY
ON PRN.city_id = CTY.id
LEFT OUTER JOIN web_application.car as CAR
ON PRN.car_id = CAR.id) AS INR
WHERE ((trim(_surname) = '' OR  lower(trim(INR.surname)) LIKE lower(trim(_surname)) || '%') AND
(trim(_name) = '' OR  lower(trim(INR.name)) LIKE lower(trim(_name)) || '%') AND
(trim(_patronymic) = '' OR  lower(trim(INR.patronymic)) LIKE lower(trim(_patronymic)) || '%') AND
(trim(_city) = '' OR  lower(trim(INR.city)) LIKE lower(trim(_city)) || '%') AND
(trim(_car) = '' OR  lower(trim(INR.car)) LIKE lower(trim(_car)) || '%')
 )
 ORDER BY INR.surname ASC, INR.name ASC, INR.patronymic ASC, INR.city ASC, INR.car ASC; 
$$
LANGUAGE sql;

/*=============�������� ��������� ��� ������ �������������� ������������ (� ������ ������ ���������� ������ ������������)=====================*/
CREATE FUNCTION web_application.users_authentificate_user
(_login varchar(100), _password varchar(100)) RETURNS TABLE (login VARCHAR(100), surname VARCHAR(100), name VARCHAR(100), patronymic VARCHAR(100)) AS 
$$
SELECT USR.login, COALESCE(USR.surname,'') as surname, COALESCE(USR.name,'') as name, COALESCE(USR.patronymic,'') as patronymic
FROM web_application.users AS USR
WHERE USR.login = _login AND USR.password = _password
$$
LANGUAGE sql;

/*=================������, ��� ��������� ������� ������ ��������==============================*/
 CREATE INDEX indx_persons_surname_name_patronymic ON web_application.persons (surname ASC, name ASC, patronymic ASC NULLS LAST);


/*����������� ��������� ������������ ������ ����������� ��� �������� ������ �����*/
/*��������� ����� �� ����������� � ��*/
GRANT CONNECT ON DATABASE search_person TO webappusr; 
/*��������� ����� �� ������ � �����*/
GRANT USAGE ON SCHEMA web_application TO webappusr; 
/*��������� ����� �� ������� �� ������*/
GRANT SELECT ON ALL TABLES IN SCHEMA web_application TO webappusr; 
/*��������� ����� �� ������ �������/��. ��������*/
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA web_application TO webappusr; 
/*��������� ����� �� ������������� �������������������*/
GRANT USAGE ON ALL SEQUENCES IN SCHEMA web_application TO webappusr; 

 /*��������� ������ ���������� ������ � ��*/
 \i D:/Projects/JavaProjects/SearchPersonSQL/sql/filldata.sql